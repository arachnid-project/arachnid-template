module Api

open Arachnid.Core
open Arachnid.Machines.Http
open Arachnid.Types.Http
open Arachnid.Routers.Uri.Template

let name_ = Route.atom_ "name"

let name =
    arachnid {
        let! nameO = Arachnid.Optic.get name_

        match nameO with
        | Some name -> return name
        | None -> return "World" }

let sayHello =
    arachnid {
        let! name = name

        return Represent.text (sprintf "Hello, %s!" name) }

let helloMachine =
    arachnidMachine {
        methods [GET; HEAD; OPTIONS]
        handleOk sayHello }

let root =
    arachnidRouter {
        resource "/hello{/name}" helloMachine }
